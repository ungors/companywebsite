<?php error_reporting(0); ?>
<div id="about" class="about-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>About eBusiness</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- single-well start-->
     <?php   
     $about = $db->prepare("select * from about where id=?;");
     $about->execute(array(1));
     $veri = $about->fetch() 
      ?>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-left">
            <div class="single-well">
              <a href="#">
                <img src="<?php echo $veri['Resim']; ?>" alt="">
              </a>
            </div>
          </div>
        </div>
        <!-- single-well end-->
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="well-middle">
            <div class="single-well">  
              <a href="#">   
                <h4 class="sec-head"><?php echo $veri['Baslik']; ?></h4>
              </a>
              <p>
              <?php echo $veri['Aciklama']; ?>
              </p>
              <ul>
                <?php foreach($db->query("select * from todo_list") as $list){ ?>
                <li>
                  <i class="fa fa-check"></i><?php echo $list['Name']; ?>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>
        <!-- End col-->
      </div>
    </div>
  </div>