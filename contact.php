<?php 
include('baglan.php'); 
error_reporting(0);
?>
<!-- Start contact Area -->
<div id="contact" class="contact-area">
    <div class="contact-inner area-padding">
      <div class="contact-overly"></div>
      <div class="container ">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="section-headline text-center">
              <h2>Contact us</h2>  
            </div>
          </div>
        </div>
        <div class="row">
          
          <?php foreach($db->query("select * from contact_ways") as $cont){ ?>
          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="contact-icon text-center">
              <div class="single-icon">
                <i class="<?php echo $cont["Simge"]; ?>"></i>
                <p>
                <?php      
                echo $cont["Aciklama"]; 
                ?>
                </p>
              </div>
            </div>
          </div>
          <?php } ?>

        </div>

        
         <div class="row">

          <!-- Start Google Map -->
          <div class="col-md-6 col-sm-6 col-xs-12">
            <!-- Start Map -->
              <div id="google-map" data-latitude="40.713732" data-longitude="-74.0092704"></div>
            <!-- End Map -->
          </div>
          <!-- End Google Map -->

          <!-- Start  contact -->
          <?php //Veritabanı!!! ?>
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="form contact-form">
               
              <div id="sendmessage">
              <?php
              $s = $_GET['sonuc'];
              if($s == "ok"){ ?>
               Mesajınız bize başarıyla iletildi :)   
              <?php   }  
              elseif($s == "no")
              { ?>
               Mesajınız iletilemedi.Lütfen tekrar deneyiniz :(
              <?php } 
              elseif($s == "bos"){ ?>
               Lütfen boş alan bırakmayınız!
              <?php }
              else
              { ?>
              Lütfen bize yazın :)
              <?php } ?>
              </div>
              

              <!-- <div id="errormessage"></div> -->
              <form action="islem.php" method="post" role="form" class="contactForm">
                <div class="form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" id="send" name="send">Send Message</button></div>
              </form>
            </div>
          </div>
          <!-- End Left contact -->
        </div>
      </div>
    </div>
  </div>
  <!-- End Contact Area -->
