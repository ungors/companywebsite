<?php include('baglan.php'); ?>

<!-- Contact bölümündeki form -->
<?php 
    if(isset($_POST['send']))
    {
        $u_name    = $_POST['name'];
        $u_email   = $_POST['email'];
        $u_subject = $_POST['subject'];
        $u_message = $_POST['message'];

        $save=$db->prepare("insert into contact set Name=?,Email=?,Subject=?,Message=?");
        $save->execute(array($u_name,$u_email,$u_subject,$u_message));
        
   

        if($save->rowCount()>0)
        {   
            
            header("Location:index.php?sonuc=ok");
        }
        else
        {
           
            header("Location:index.php?sonuc=no");
        }

        if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['subject']) || empty($_POST['message']))
        {
            $sonuc="bos";
            header("Location:index.php?sonuc=bos");
        }
    }
?>

<!-- Login Kontrol -->

<?php

    $log=$db->prepare("select * from admin where id=?;");
    $log->execute(array("1"));
    $data=$log->fetch(); 
    $uname = $data['Username'];
    $pass = $data['Password'];

    if(isset($_POST['login']))
    {
        if($_POST['username']==$uname && $_POST['password'] == $pass)
        {
           session_start();
           $_SESSION['admin'] = "sami";
           header("Location:admin/index.php"); 
        }

        else
        {
            //Kullanıcı adı veya şifre yanlış mesajı göster!!!
            //Eğer boş alan bırakılmışsa

            if(empty($_POST['username']) || empty($_POST['password']))
            {
                header("Location:admin/login.php?sonuc=bos");
            }

            else
            {
                header("Location:admin/login.php?sonuc=yanlis");
            }

        }

        //Eğer boş alan bırakılmışsa
    }

?>

<!-- Menü güncelleme -->

<?php

      if(isset($_POST['duzenle']))
      {
       $i = 0;
       foreach($_POST['menu_txt'] as $menutx) 
       {
        $menutx = $_POST['menu_txt'][$i];

        $id = $_POST['menu_id'][$i];
        $stmt = $db->prepare("update menu set Name=:name WHERE Id=:id");

        $stmt->bindValue(':name', $menutx, PDO::PARAM_STR);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->execute();
        $i++;
       }   

       if($stmt)
       {
           header("Location:admin/pages/header/menuduzenle.php");
       }
       else
       {
           header("Location:admin/index.php");
       }
      }

?>

<!-- Slider Güncelleme -->

<?php

      if(isset($_POST['sliderduzenle']))
      {
          $a = 0;
          foreach($_POST['Baslik'] as $bas)
          {
              $bas    = $_POST['Baslik'][$a];
              $acikla = $_POST['Aciklama'][$a];
              $img    = $_POST['Image'][$a];
              $id     = $_POST['slider_id'][$a];

              $kaydet = $db->prepare("update header set Baslik=:baslik, Aciklama=:aciklama, Resim=:resim where Id=:id");
              
              $kaydet->bindValue(':baslik', $bas, PDO::PARAM_STR);
              $kaydet->bindValue(':aciklama', $acikla, PDO::PARAM_STR);
              $kaydet->bindValue(':resim', $img, PDO::PARAM_STR);
              $kaydet->bindValue(':id', $id, PDO::PARAM_INT);

              $kaydet->execute();
              $a++;
          }

          if($kaydet)
          {
              header("Location:admin/pages/header/sliderduzenle.php");
          }
          else
          {
              header("Location:admin/index.php");
          }
      }

?>

<!-- About sayfası düzenleme -->

<?php 

      if(isset($_POST['aboutduzenle']))
      {
          $bas  = $_POST['baslik'];
          $acik = $_POST['aciklama'];
          $img  = $_POST['resim'];

          
          $save = $db->prepare("update about set Baslik=:baslik, Aciklama=:aciklama, Resim=:resim where id=1");
          $save->bindValue(':baslik',$bas,PDO::PARAM_STR);
          $save->bindValue(':aciklama',$acik,PDO::PARAM_STR);
          $save->bindValue(':resim',$img,PDO::PARAM_STR);
          
          $save->execute();
          
          

          $d=0;
          foreach($_POST['todo'] as $tol)
          {
              $tol = $_POST['todo'][$d];
              $id  = $_POST['about_id'][$d];

              $save_tl = $db->prepare("update todo_list set Name=:name where Id=:id");
              $save_tl->bindValue(':name',$tol,PDO::PARAM_STR);
              $save_tl->bindValue(':id',$id,PDO::PARAM_INT);

              $save_tl->execute();
              $d++;
          }

          if($save && $save_tl)
          {
              header("Location:admin/pages/about/aboutguncelle.php");
          }
          else
          {
              header("Location:admin/index.php");
          }
      }

?>

<!-- Service sayfasının düzenlenmesi -->

<?php

    if(isset($_POST['serviceduzenle']))
    {

        $i=0;
        foreach($_POST['simge'] as $sim)
        {
            $sim = $_POST['simge'][$i];
            $bas = $_POST['baslik'][$i];
            $ac = $_POST['aciklama'][$i];
            $id  = $_POST['service_id'][$i];

            $update_sim = $db->prepare("update services set Simge=:s_simge, Baslik=:s_baslik, Aciklama=:s_aciklama where Id=:id");

            $update_sim->bindValue(':s_simge',$sim,PDO::PARAM_STR);
            $update_sim->bindValue(':s_baslik',$bas,PDO::PARAM_STR);
            $update_sim->bindValue(':s_aciklama',$ac,PDO::PARAM_STR);
            $update_sim->bindValue(':id',$id,PDO::PARAM_INT);

            $update_sim->execute();
            $i++;
        }

       if($update_sim)
        {
            header("Location:admin/pages/services/serviceguncelle.php");
        }
        else
        {
            header("Location:admin/index.php");
        }

    }

?>

<!-- Skill bölümünün yapımı -->

<?php 

    if(isset($_POST['skillduzenle']))
    {
        $j=0;
        foreach($_POST['adi'] as $ad)
        {
            $ad    = $_POST['adi'][$j];
            $yuzde = $_POST['yuzde'][$j];
            $id    = $_POST['skill_id'][$j];

            $save_skill = $db->prepare("update skills set Adi=:adi,Yuzde=:yuzde where Id=:id");
            $save_skill -> bindValue(':adi',$ad,PDO::PARAM_STR);
            $save_skill -> bindValue(':yuzde',$yuzde,PDO::PARAM_INT);
            $save_skill -> bindValue(':id',$id,PDO::PARAM_INT);  

            $save_skill -> execute();
            $j++;
        }

        if($save_skill)
        {
            header("Location:admin/pages/services/skillguncelle.php");
        }
    }

?>

<!-- Team sayfası yapımı -->
<?php 

    if(isset($_POST['teamduzenle']))
    {
        $t=0;
        foreach($_POST['adi'] as $ad)
        {
            $ad    = $_POST['adi'][$t];
            $gorev = $_POST['gorevi'][$t];
            $img   = $_POST['foto'][$t];
            $id    = $_POST['team_id'][$t];

            $save_team = $db->prepare("update team set Adi=:adi, Gorevi=:gorevi, Foto=:foto where Id=:id");
            $save_team->bindValue(':adi',$ad,PDO::PARAM_STR);
            $save_team->bindValue(':gorevi',$gorev,PDO::PARAM_STR);
            $save_team->bindValue(':foto',$img,PDO::PARAM_STR);
            $save_team->bindValue(':id',$id,PDO::PARAM_INT);

            $save_team->execute();
            $t++;

            if($save_team)
            {
                header("Location:admin/pages/team/teamguncelle.php");
            }
        }
    }


    if(isset($_POST['linkduzenle']))
    {
        $r=0;
        foreach($_POST['face_url'] as $face)
        {
            $face  = $_POST['face_url'][$r];
            $twt   = $_POST['twt_url'][$r];
            $insta = $_POST['insta_url'][$r];
            $id    = $_POST['team_id'][$r];

            $save_link = $db->prepare("update team set Facebook=:face, Twitter=:twitter, Instagram=:instagram where Id=:id");
            $save_link->bindValue(':face',$face,PDO::PARAM_STR);
            $save_link->bindValue(':twitter',$twt,PDO::PARAM_STR);
            $save_link->bindValue(':instagram',$insta,PDO::PARAM_STR);
            $save_link->bindValue(':id',$id,PDO::PARAM_INT);

            $save_link->execute();
            $r++;

            if($save_link)
            {
                header("Location:admin/pages/team/linkguncelle.php");
            }
        }
    }

?>

<!-- Contact-Ways Yapımı -->

<?php

    if(isset($_POST['contactduzenle']))
    {

        $u=0;
        foreach($_POST['simge'] as $c_simge)
        {
            $c_simge    = $_POST['simge'][$u];
            $c_aciklama = $_POST['aciklama'][$u];
            $c_id       = $_POST['contact_id'][$u];

            $c_update = $db->prepare("update contact_ways set Simge=:simge, Aciklama=:aciklama where Id=:id");
            $c_update->bindValue(':simge',$c_simge,PDO::PARAM_STR);
            $c_update->bindValue(':aciklama',$c_aciklama,PDO::PARAM_STR);
            $c_update->bindValue(':id',$c_id,PDO::PARAM_INT);

            $c_update->execute();
            $u++;
        }

        if($c_update)
        {
            header("Location:admin/pages/contact/contactguncelle.php");
        }

    }

?>

<!-- Admin Güncelleştirme Sayfası -->

<?php

    if(isset($_POST['adminduzenle']))
    {

        $e=0;
        foreach($_POST['username'] as $user)
        {
            $user = $_POST['username'][$e];
            $pass = $_POST['password'][$e];
            $id   = $_POST['admin_id'][$e];

            $admin_update = $db->prepare("update admin set Username=:username,Password=:password where Id=:id");
            $admin_update->bindValue(':username',$user,PDO::PARAM_STR);
            $admin_update->bindValue(':password',$pass,PDO::PARAM_STR);
            $admin_update->bindValue(':id',$id,PDO::PARAM_INT);

            $admin_update->execute();
            $e++;
        }
    
    /*
            $user = $_POST['username'];
            $pass = $_POST['password'];
            $id   = $_POST['admin_id'];
            $admin_update =$db->prepare("update admin set Username=:username, Password=:password where Id=:id");
            $admin_update->bindValue(':username',$user,PDO::PARAM_STR);
            $admin_update->bindValue(':password',$pass,PDO::PARAM_STR);
            $admin_update->bindValue(':id',$id,PDO::PARAM_INT);
     */
        if($admin_update)
        {
            header("Location:admin/pages/admin/admin_option.php");
        }
    }

?>