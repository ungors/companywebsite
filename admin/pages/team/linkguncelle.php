<?php include("../pageheader.php"); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../../index.php">
          <i class="fa fa-home" aria-hidden="true"></i>
            <span>Anasayfa</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Header</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../header/menuduzenle.php"><i class="far fa-circle"></i> Menü</a></li>
            <li><a href="../header/sliderduzenle.php"><i class="far fa-circle"></i> Slider</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>About</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../about/aboutguncelle.php"><i class="far fa-circle"></i> About</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Services</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../services/serviceguncelle.php"><i class="far fa-circle"></i> Services</a></li>
            <li><a href="../services/skillguncelle.php"><i class="far fa-circle"></i> Skills</a></li>
         </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Team</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="teamguncelle.php"><i class="far fa-circle"></i>Team</a></li>
            <li><a href="linkguncelle.php"><i class="far fa-circle"></i>Social Links</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Contact</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../contact/messages.php"><i class="fas fa-envelope"></i> Messages</a></li>
            <li><a href="../contact/contactguncelle.php"><i class="far fa-circle"></i> Contacts Option</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Admin</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../admin/admin_option.php"><i class="far fa-circle"></i>Admin Option</a></li>
          </ul>
        </li>
        
    </section>
    <!-- /.sidebar -->
  </aside>

 <div class="content-wrapper">
    <div class="container">
    <div class="row">
        <div class="col-md-12" style="height:20px;"></div>
    </div>
    <div class="row">
  <form action="../../../islem.php" method="POST" class="form-inline">
        <?php foreach($db->query("select * from team")as $tm){ ?>
            <div class="col-md-1">
        <input type="text" name="team_id[]" style="visibility:hidden;" value="<?php echo $tm['Id']; ?>">  
            </div>
  <div class="col-md-3">
    <label>Facebook:</label>
    <input type="text" class="form-control" name="face_url[]" value="<?php echo $tm['Facebook']; ?>">
  </div>
  <div class="col-md-4">
    <label>Twitter:</label>
    <input type="text" class="form-control" name="twt_url[]" value="<?php echo $tm['Twitter']; ?>">
  </div>
  <div class="col-md-4">
  <label>Instagram:</label>
  <input type="text" class="form-control" name="insta_url[]" value="<?php echo $tm['Instagram']; ?>">
  </div>
  <div class="row">
      <div class="col-md-12" style="height:30px;"></div>
   </div>
        <?php } ?>
  <input type="submit" name="linkduzenle" style="float:left; margin-left:110px; width:935px;" class="btn btn-primary" value="Güncelle">
  </form>
    
  </div>
  </div>
  </div>


<?php include("../pagefooter.php"); ?>
