<?php include("../pageheader.php"); ?>

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../../index.php">
          <i class="fa fa-home" aria-hidden="true"></i>
            <span>Anasayfa</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Header</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="menuduzenle.php"><i class="far fa-circle"></i> Menü</a></li>
            <li><a href="sliderduzenle.php"><i class="far fa-circle"></i> Slider</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>About</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../about/aboutguncelle.php"><i class="far fa-circle"></i> About</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Services</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../services/serviceguncelle.php"><i class="far fa-circle"></i> Services</a></li>
            <li><a href="../skillguncelle.php"><i class="far fa-circle"></i> Skills</a></li>
         </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Team</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../team/teamguncelle.php"><i class="far fa-circle"></i>Team</a></li>
            <li><a href="../team/linkguncelle.php"><i class="far fa-circle"></i>Social Links</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Contact</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../contact/messages.php"><i class="fas fa-envelope"></i> Messages</a></li>
            <li><a href="../contact/contactguncelle.php"><i class="far fa-circle"></i> Contacts Option</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Admin</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../admin/admin_option.php"><i class="far fa-circle"></i>Admin Option</a></li>
          </ul>
        </li>
        
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
     
      <div class="box box-warning">
            <div class="box-header with-border">
              <h2 class="box-title">Menü İsimleri</h2>
            </div>
            <!-- /.box-header -->
        <div class="box-body">
              <form action="../../../islem.php" method="POST" role="form">
                <div class="container">
                <div class="row">
                <?php foreach($db->query("select * from menu") as $m){ ?>
                <input style="margin-top:30px; text-align:center;" name="menu_txt[]" class="col-md-7" type="text" class="form-control" placeholder="Enter ..." value="<?php echo $m['Name']; ?>">
                <input name="menu_id[]" type="text" class="col-md-3" style="visibility: hidden;" value="<?php echo $m['Id'];?>">
                <?php } ?>
               <div class=col-md-12> <input class="btn btn-primary btn-lg" style="margin-left:-15px; margin-top:30px; float:left; width:684px;" type="submit" name="duzenle" value="Düzenle"></div>
                </div>
                </div>
              </form>
    
    </section>
  </div>
  

  <?php include("../pagefooter.php"); ?>


