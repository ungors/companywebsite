<?php include("../pageheader.php"); ?>

<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li>
          <a href="../../index.php">
          <i class="fa fa-home" aria-hidden="true"></i>
            <span>Anasayfa</span>
            <span class="pull-right-container">
            </span>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Header</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../header/menuduzenle.php"><i class="far fa-circle"></i> Menü</a></li>
            <li><a href="../header/sliderduzenle.php"><i class="far fa-circle"></i> Slider</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>About</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../about/aboutguncelle.php"><i class="far fa-circle"></i> About</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Services</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../services/serviceguncelle.php"><i class="far fa-circle"></i> Services</a></li>
            <li><a href="../skillguncelle.php"><i class="far fa-circle"></i> Skills</a></li>
         </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Team</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../team/teamguncelle.php"><i class="far fa-circle"></i>Team</a></li>
            <li><a href="../team/linkguncelle.php"><i class="far fa-circle"></i>Social Links</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Contact</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="messages.php"><i class="fas fa-envelope"></i> Messages</a></li>
            <li><a href="contactguncelle.php"><i class="far fa-circle"></i> Contacts Option</a></li>
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
          <i class="fa fa-angle-right"></i>
            <span>Admin</span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../admin/admin_option.php"><i class="far fa-circle"></i>Admin Option</a></li>
          </ul>
        </li>
        
    </section>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper">
    <div class="container">
    <div class="row">
        <div class="col-md-12" style="height:20px;"></div>
    </div>
    <div class="row">
    <table class="table table-light">
  <thead class="thead">
    <tr>
      <th scope="col">Id</th>
      <th scope="col">Name</th>
      <th scope="col">E-mail</th>
      <th scope="col">Subject</th>
      <th scope="col">Message</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($db->query("select * from contact") as $con){ ?>
    <tr>
      <th scope="row"><?php echo $con["Id"]; ?></th>
      <td><?php echo $con["Name"]; ?></td>
      <td><?php echo $con["Email"]; ?></td>
      <td><?php echo $con["Subject"]; ?></td>
      <td><?php echo $con["Message"]; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
    </div>
   </div>
   </div>

<?php include("../pagefooter.php"); ?>