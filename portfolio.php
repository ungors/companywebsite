<?php include('baglan.php'); ?>
<div id="portfolio" class="portfolio-area area-padding fix">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Our Portfolio</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Start Portfolio -page -->
        <div class="awesome-project-1 fix">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="awesome-menu ">
              <ul class="project-menu">
                <li>
                  <a href="#" class="active" data-filter="*">All</a>
                </li>
                <li>
                  <a href="#" data-filter=".development">Development</a>
                </li>
                <li>
                  <a href="#" data-filter=".design">Design</a>
                </li>
                <li>
                  <a href="#" data-filter=".photo">Photoshop</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <?php foreach($db->query("select * from portfolio") as $port){ ?>
        <div class="awesome-project-content">
          <!-- single-awesome-project start -->
          <div class="col-md-4 col-sm-4 col-xs-12 <?php echo $port['Tur']; ?>">
            <div class="single-awesome-project">
              <div class="awesome-img">
                <a href="#"><img src="<?php echo $port['Foto']; ?>" alt="" /></a>
                <div class="add-actions text-center">
                  <div class="project-dec">
                    <a class="venobox" data-gall="myGallery" href="<?php echo $port['Foto']; ?>">
                      <h4><?php echo $port['Adi']; ?></h4>
                      <span><?php echo $port['Alan']; ?></span>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <?php } ?>
         </div>
          </div>
           </div>
            </div>