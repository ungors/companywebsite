<?php include('baglan.php'); ?>

<div id="team" class="our-team-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline text-center">
            <h2>Our special Team</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="team-top">
        <?php foreach($db->query("select * from team") as $team){ ?>
          <div class="col-md-3 col-sm-3 col-xs-12">
            <div class="single-team-member">
              <div class="team-img">
                <a href="#">
										<img src="<?php echo $team['Foto']; ?>" alt="">
									</a>
                <div class="team-social-icon text-center">
                  <ul>
                    <li>
                      <a href="<?php echo $team['Facebook']; ?>">
													<i class="fa fa-facebook"></i>
												</a>
                    </li>
                    <li>
                      <a href="<?php echo $team['Twitter']; ?>">
													<i class="fa fa-twitter"></i>
												</a>
                    </li>
                    <li>
                      <a href="<?php echo $team['Instagram']; ?>">
													<i class="fa fa-instagram"></i>
												</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="team-content text-center">
                <h4><?php echo $team['Adi']; ?></h4>
                <p><?php echo $team['Gorevi']; ?></p>
              </div>
            </div>
          </div>
            <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <div class="reviews-area hidden-xs">
    <div class="work-us">
      <div class="work-left-text">
        <a href="#">
						<img src="img/about/2.jpg" alt="">
					</a>
      </div>
      <div class="work-right-text text-center">
        <h2>working with us</h2>
        <h5>Web Design, Ready Home, Construction and Co-operate Outstanding Buildings.</h5>
        <a href="#contact" class="ready-btn">Contact us</a>
      </div>
    </div>
  </div>