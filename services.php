<?php include('baglan.php'); ?>
<div id="services" class="services-area area-padding">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="section-headline services-head text-center">
            <h2>Our Services</h2>
          </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="services-contents">
          

  <?php  foreach($db->query("select * from services") as $row){  ?>

          <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="about-move">
              <div class="services-details">
                <div class="single-services">
                  <a class="services-icon" href="#">
                    <i class="<?php echo $row['Simge']; ?>"></i>
                  </a>
                  <h4><?php echo $row['Baslik']; ?></h4>
                  <p>
                    <?php echo $row['Aciklama']; ?>
                  </p>
                </div>
              </div>
            </div>
          </div>

  <?php } ?>



        </div>
      </div>
    </div>
  </div>